
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sixdee.imp.helper.Globals;
import com.sixdee.inclient.util.STSUtil;

public class MMLBilling {

	private Logger log = LogManager.getLogger(MMLBilling.class.getName());
	private InputStream				is					= null;
	private OutputStream			os					= null;
	private Socket					socket				= null;
	private String					hostName			= Globals.in_server_host;
	private int						port				= Globals.in_server_port;
	private Map 					map 				= null;
	private InetAddress 			addr				= null;
	private SocketAddress 			sockaddr 			= null;


	private final int	LOGIN_OPERATIONCMD_ID	= 1;
	private final int	LOGOUT_OPERATIONCMD_ID	= 2;
	private final int	DEFAULT_OPERATIONCMD_ID	= 3;

	int sessionId=10;
	int transactionId=900000;

	public static void main(String a[]){
		try {
			@SuppressWarnings("unused")
			Map map = new MMLBilling().billing("24720450","-100", "","");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@SuppressWarnings("unchecked")
	public Map billing(String msisdn, String chargeAmount, String comments, String command) throws Exception{
		try{

			addr = InetAddress.getByName(hostName);
			sockaddr = new InetSocketAddress(addr, port);

			// Create an unbound socket
			socket = new Socket();

			// This method will block no more than timeoutMs.
			// If the timeout occurs, SocketTimeoutException is thrown.
			int timeoutMs = 200000; // 2 seconds
			socket.connect(sockaddr, timeoutMs);

			os = socket.getOutputStream();
			is = socket.getInputStream();
			log.info(""+os);

			login();
			log.info(msisdn+"::Login Successfull");

			List list = new ArrayList();
			list.add(0, msisdn);//0025224720450

			if(chargeAmount != null)
				list.add(1, chargeAmount);
			if(comments != null)
				list.add(2, comments);

			map = executeCommand(command, list);
			log.info(msisdn+":: ExecuteCommand Response:"+printf(map));

			try{
				logout();
				log.info(msisdn+"::Logout Successfull");
			}catch (Exception e) {
				// TODO: handle exception
			}

		} catch (UnknownHostException uhe) {
			log.fatal("Don't know about host " + hostName);
			uhe.printStackTrace();
			throw uhe;
		} catch (SocketTimeoutException e) {
			log.fatal("Timeout occurs " + hostName);
			e.printStackTrace();
			throw e;
		} catch (IOException ioe) {
			log.fatal("Couldn't get I/O for the connection to the host " + hostName);
			ioe.printStackTrace();
			throw ioe;
		} catch (Exception e) {
			log.fatal("Couldn't get the connection to the host " + hostName);
			e.printStackTrace();
			throw e;
		}finally{
			addr = null;
			sockaddr = null;
			try {
				if (os != null)
					os.close();

				os = null;
			} catch (IOException e) {}
			try {
				if (is != null)
					is.close();

				is = null;
			} catch (IOException e) {}
			try {
				if (socket != null)
					socket.close();

				socket = null;
			} catch (IOException e) {}
		}

		return map;

	}


	public Map executeCommand(String opCommand, List attrs) throws IOException, Exception {
		// executeCommand command have to send

		try {

			// 1.2.5 Message Start Flag (4 Byte)
			byte msgStartFlag[] = msgStartFlagBuilder();

			// 1.2.7 Message Header (20 Byte for V1.00){Version number[4]+Terminal identifier[8]+Service name[8]}
			byte msgHeader[] = msgHeaderBuilder("1.00", "internal", opCommand, DEFAULT_OPERATIONCMD_ID);

			// 1.2.8 Session Header (18 Byte){Session ID[8]+Session control character[6]+Reserved field[4]}
			byte sessionHeader[] = sessionHeaderBuilder("DLGCON", "");

			// 1.2.9 Transaction Header (18 Byte){}
			byte transactionHeader[] = transactionHeaderBuilder("TXBEG", "");

			// 1.2.10 Operative Information
			// Fill with blank spaces if the length of command is not the multiple of 4
			byte operativeInfo[] = operationInfoBuilder(opCommand, attrs);

			// 1.2.11 Checksum (8 Byte){Perform XOR for "Message header + Session header + Transaction header + Operative information"}
			byte checkSum[] = checkSumBuilder(msgHeader, sessionHeader, transactionHeader, operativeInfo);

			// 1.2.6 Message Length (4 Byte)
			byte msgLength[] = msgLengthBuilder(msgHeader, sessionHeader, transactionHeader, operativeInfo);

			byte msgS[] = msgBuilder(msgStartFlag, msgLength, msgHeader, sessionHeader, transactionHeader, operativeInfo, checkSum);

			log.debug(printf(msgS));
			log.info(new String(msgS));

			log.info("OperationCommand Msg is sending to Server.");
			byte b2[] = operationCall(msgS);

			log.debug("Receive as OperationCommand Response : : " + printf(b2));
			log.info("Receive as OperationCommand Response : : " + new String(b2));

			Map map = operationRespParser(b2);

			log.debug(printf(map));
			String RETN = (String) map.get("RETN");
			if (RETN != null && RETN.equals("0")) {
				log.debug("RETN : " + RETN);
			}

			return map;
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw e;
		}
	}

	private String getServiceName(String cmd, int sid) {
		String sName = null;
		switch (sid) {
		case LOGIN_OPERATIONCMD_ID:
		case LOGOUT_OPERATIONCMD_ID:
			// Only for Login and Logout Command
			sName = "SRVM";
			break;
		default:
			if (cmd != null) {
				StringTokenizer st = new StringTokenizer(cmd, ":");
				if (st.hasMoreElements()) {
					st = new StringTokenizer((String) st.nextElement());
					int i = 0;
					while (st.hasMoreTokens()) {
						if (++i == 2) {
							sName = ((String) st.nextElement()).trim();
							break;
						}
						st.nextElement();
					}
				}
			}
		}
		return (sName != null ? sName : "SRVM");
	}
	protected boolean logout() throws IOException, Exception {
		// Login command have to send

		try {

			// 1.2.5 Message Start Flag (4 Byte)
			byte msgStartFlag[] = msgStartFlagBuilder();

			// 1.2.7 Message Header (20 Byte for V1.00){Version number[4]+Terminal identifier[8]+Service name[8]}
			byte msgHeader[] = msgHeaderBuilder("1.00", "internal", null, LOGOUT_OPERATIONCMD_ID);

			// 1.2.8 Session Header (18 Byte){Session ID[8]+Session control character[6]+Reserved field[4]}
			byte sessionHeader[] = sessionHeaderBuilder("DLGCON", "");

			// 1.2.9 Transaction Header (18 Byte){}
			byte transactionHeader[] = transactionHeaderBuilder("TXBEG", "");

			// 1.2.10 Operative Information
			// Fill with blank spaces if the length of command is not the multiple of 4
			byte operativeInfo[] = operationInfoBuilder(Globals.logout_command, null);

			// 1.2.11 Checksum (8 Byte){Perform XOR for "Message header + Session header + Transaction header + Operative information"}
			byte checkSum[] = checkSumBuilder(msgHeader, sessionHeader, transactionHeader, operativeInfo);

			// 1.2.6 Message Length (4 Byte)
			byte msgLength[] = msgLengthBuilder(msgHeader, sessionHeader, transactionHeader, operativeInfo);

			byte msgS[] = msgBuilder(msgStartFlag, msgLength, msgHeader, sessionHeader, transactionHeader, operativeInfo, checkSum);

			log.debug(printf(msgS));
			log.info(new String(msgS));

			log.info("Logout Msg is sending to Server.");
			byte b2[] = operationCall(msgS);

			log.debug("Receive as Logout Response : : " + printf(b2));
			log.info("Receive as Logout Response : : " + new String(b2));

			Map map = operationRespParser(b2);

			String RETN = (String) map.get("RETN");
			if (RETN != null && RETN.equals("0")) {
				log.debug("RETN : " + RETN);
				return true;
			}
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw e;
		}

		return false;
	}
	@SuppressWarnings("unchecked")
	private Map operationRespParser(byte[] b2) {
		HashMap map = new HashMap();
		if (b2 != null && b2.length > 56) {

			String testrespMsg = new String(b2);
			log.info("testrespMsg::::::::::::::::::::>>>>>>>>>>>"+testrespMsg);


			//String respMsg = new String(b2, 64, b2.length - 64);
			//String respMsg = new String(b2, 60, b2.length-60 );

			String respMsg = new String(b2 );
			log.info("respMsg>>>>>>>>>>>"+respMsg);
			StringTokenizer st = new StringTokenizer(respMsg, ":");
			List msgList = new ArrayList();
			for (int i = 0; st.hasMoreElements(); i++) {
				msgList.add((String) st.nextElement());
			}

			for (int i = 0; i < msgList.size(); i++) {
				String msg = (String) msgList.get(i);
				log.info("msg>>>>>>>>>>>"+msg);
				if (msg != null && msg.contains("RETN")) {
					st = new StringTokenizer(msg, "=,");
					List attr = new ArrayList();
					List result = new ArrayList();
					while (st.hasMoreElements()) {
						String key = st.nextToken();
						String val = st.nextToken();
						if (key != null && key.trim().equalsIgnoreCase("ATTR")) {
							StringTokenizer stt = new StringTokenizer(val, "&");
							while (stt.hasMoreElements()) {
								attr.add((String) stt.nextElement());
							}
						} else if (key != null && key.trim().equalsIgnoreCase("RESULT")) {
							String del = "|";
							StringTokenizer stt = new StringTokenizer(val, del, true);
							String preTok = null;
							String tok = null;
							while (stt.hasMoreTokens()) {
								tok = stt.nextToken();
								if (tok.equals(del) && preTok != null && preTok.equals(del)) {
									result.add(null);
								} else if (tok.equals(del) && preTok != null && !preTok.equals(del)) {
									//
								} else {
									result.add(tok);
								}
								preTok = tok;
							}
						} else {
							map.put(key.trim(), val);
						}
					}

					for (int j = 0; j < attr.size(); j++) {
						map.put(((String) attr.get(j)).trim(), (String) result.get(j));
					}
					break;
				}
			}
		}

		return map;
	}
	private byte[] msgStartFlagBuilder() {
		byte msgStartFlag[] = new byte[4];
		String msgStartFlagV = "`SC`";
		STSUtil.mcfn_encodeStringIntoByte(msgStartFlagV, msgStartFlag, 0x00, 0x04);

		return msgStartFlag;
	}
	private byte[] msgLengthBuilder(byte[] msgHeader, byte[] sessionHeader, byte[] transactionHeader, byte[] operativeInfo) {
		// 1.2.6 Message Length (4 Byte)
		byte msgLength[] = new byte[4];
		int msgLengthV = msgHeader.length + sessionHeader.length + transactionHeader.length + operativeInfo.length;
		STSUtil.mcfn_encodeNStringIntoByte(Integer.toHexString(msgLengthV), msgLength, 0x00, 0x04);

		return msgLength;
	}

	private byte[] msgHeaderBuilder(String versionNumV, String terminalIdentifierV, String opCmd, int opId) {
		// 1.2.7 Message Header (20 Byte for V1.00){Version number[4]+Terminal identifier[8]+Service name[8]}
		byte msgHeader[] = new byte[20];
		int Length = 0x00;
		STSUtil.mcfn_encodeStringIntoByte(versionNumV, msgHeader, Length, 0x04);
		Length += 0x04;
		STSUtil.mcfn_encodeStringIntoByte(terminalIdentifierV, msgHeader, Length, 0x08);
		Length += 0x08;
		String serviceNameV = getServiceName(opCmd, opId);
		STSUtil.mcfn_encodeStringIntoByte(serviceNameV, msgHeader, Length, 0x08);

		return msgHeader;
	}

	private byte[] sessionHeaderBuilder(String sessionControlCharV, String reservedFieldV) {
		// 1.2.8 Session Header (18 Byte){Session ID[8]+Session control character[6]+Reserved field[4]}
		byte sessionHeader[] = new byte[18];
		int Length = 0x00;
		String sessionIdV = Long.toHexString(sessionId);
		STSUtil.mcfn_encodeNStringIntoByte(sessionIdV, sessionHeader, Length, 0x08);
		Length += 0x08;
		STSUtil.mcfn_encodeStringIntoByte(sessionControlCharV, sessionHeader, Length, 0x06);
		Length += 0x06;
		STSUtil.mcfn_encodeNStringIntoByte(reservedFieldV, sessionHeader, Length, 0x04);

		return sessionHeader;
	}

	private byte[] transactionHeaderBuilder(String transactionControlCharV, String reservedFieldV) {
		// 1.2.9 Transaction Header (18 Byte){}
		byte transactionHeader[] = new byte[18];
		int Length = 0x00;

		//to generate transId
		String transactionIdV = Long.toHexString(TxIDGenerator.getInstance().getLongTransactionId());
		STSUtil.mcfn_encodeNStringIntoByte(transactionIdV, transactionHeader, Length, 0x08);
		Length += 0x08;
		STSUtil.mcfn_encodeStringIntoByte(transactionControlCharV, transactionHeader, Length, 0x06);
		Length += 0x06;
		STSUtil.mcfn_encodeNStringIntoByte(reservedFieldV, transactionHeader, Length, 0x04);

		return transactionHeader;
	}

	private byte[] operationInfoBuilder(String operationCmd, List attrs) {
		// 1.2.10 Operative Information
		if (attrs != null && !attrs.isEmpty()) {
			MessageFormat mf = new MessageFormat(operationCmd);
			int size = attrs.size();
			Object obj[] = new Object[size];
			for (int i = 0; i < size; i++) {
				obj[i] = attrs.get(i);
			}
			operationCmd = mf.format(obj);
		}

		// Fill with blank spaces if the length of command is not the multiple of 4
		int m = (operationCmd.getBytes().length % 4);
		byte operativeInfo[] = new byte[operationCmd.getBytes().length + (m != 0 ? 4 - m : 0)];
		StringBuffer sb = new StringBuffer();
		sb.append(operationCmd);
		for (int i = m; i != 0 && i < 4; i++) {
			sb.append(" ");
		}
		operationCmd = sb.toString();
		STSUtil.mcfn_encodeStringIntoByte(operationCmd, operativeInfo, 0x00);

		return operativeInfo;
	}
	private byte[] msgBuilder(byte[] msgStartFlag, byte[] msgLength, byte[] msgHeader, byte[] sessionHeader, byte[] transactionHeader, byte[] operativeInfo, byte[] checkSum) {
		byte msgS[] = new byte[msgStartFlag.length + msgLength.length + msgHeader.length + sessionHeader.length + transactionHeader.length + operativeInfo.length + checkSum.length];
		int Length = 0x00;

		System.arraycopy(msgStartFlag, 0x00, msgS, Length, msgStartFlag.length);
		Length += msgStartFlag.length;

		System.arraycopy(msgLength, 0x00, msgS, Length, msgLength.length);
		Length += msgLength.length;

		System.arraycopy(msgHeader, 0x00, msgS, Length, msgHeader.length);
		Length += msgHeader.length;

		System.arraycopy(sessionHeader, 0x00, msgS, Length, sessionHeader.length);
		Length += sessionHeader.length;

		System.arraycopy(transactionHeader, 0x00, msgS, Length, transactionHeader.length);
		Length += transactionHeader.length;

		System.arraycopy(operativeInfo, 0x00, msgS, Length, operativeInfo.length);
		Length += operativeInfo.length;

		System.arraycopy(checkSum, 0x00, msgS, Length, checkSum.length);
		Length += checkSum.length;

		return msgS;
	}

	private byte[] operationCall(byte[] msgS) throws IOException {
		byte b1[] = new byte[8];
		byte b2[] = null;
		byte b3[] = new byte[24];
		@SuppressWarnings("unused")
		byte b4[] = new byte[4];

		try {
			os.write(msgS);
			os.flush();


			is.read(b3, 0, 24);
			log.info("sb1>>> "+b3);
			int totalLength  = is.available();
			log.info("totalLength>>> "+totalLength);

			StringBuffer sb1 = new StringBuffer();
			for (int i = 0; i < b3.length; i++) {
				sb1.append(b3[i]);
				sb1.append(" ");
			}
			log.info("sb1>>> "+sb1);

			is.read(b1, 0, 8);
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < b1.length; i++) {
				sb.append(b1[i]);
				sb.append(" ");
			}
			System.out.println("b1>>>>>>>>>>>."+b1);
			log.info("resp for first 8bit  "+sb);
			log.info("b1."+b1);

			//optional

			//			is.read(b4, 4, 8);
			//			StringBuffer sb3 = new StringBuffer();
			//			for (int i = 0; i < b4.length; i++) {
			//				sb3.append(b4[i]);
			//				sb3.append(" ");
			//			}
			//			System.out.println("b4>>>>>>>>>>>."+b4);
			//			log.info("resp for 4bit to 8bit  "+sb3);
			//			log.info("b4."+b4);
			int length=0;

			length = STSUtil.mcfn_encodeHexaStringIntoInt(new String(b1, 4, 4));
			log.info("length"+length);
			log.info("totalLength"+totalLength);
			//b2 = new byte[8 + 1200 + 8];

			//This has been added newly
			//if(totalLength==0)
			//b2=new byte[length];
			//else
			if(totalLength>2000)
				b2=new byte[totalLength];
			else
				b2=new byte[2000];
			//System.arraycopy(b1, 0, b2, 0, 8);
			//is.read(b2, 0, 8);

			/*if(totalLength==0)
				is.read(b2, 0, length);
			else
				is.read(b2, 0, b2.length);*/
			is.read(b2, 0, b2.length);
			log.info("b2>>>>"+b2);

			String testrespMsg = new String(b2);
			System.out.println("operationCall>>>>>>>>>>>"+testrespMsg);
		} catch (IOException e) {
			log.error("Message is "+e.getMessage());
			e.printStackTrace();
			//closed = true;
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			//			closed = true;
			throw new IOException(e.getMessage());
		} finally {

		}
		//	log.info("b2>>>>>>>>>>>."+b2);
		System.out.println("b2>>>>>>>>>>>."+b2);
		return b2;
	}

	protected boolean login() throws IOException, Exception {
		// Login command have to send

		try {

			// 1.2.5 Message Start Flag (4 Byte)
			byte msgStartFlag[] = msgStartFlagBuilder();

			// 1.2.7 Message Header (20 Byte for V1.00){Version number[4]+Terminal identifier[8]+Service name[8]}
			byte msgHeader[] = msgHeaderBuilder("1.00", "internal", null, LOGIN_OPERATIONCMD_ID);

			// 1.2.8 Session Header (18 Byte){Session ID[8]+Session control character[6]+Reserved field[4]}
			byte sessionHeader[] = sessionHeaderBuilder("DLGLGN", "");

			// 1.2.9 Transaction Header (18 Byte){}
			byte transactionHeader[] = transactionHeaderBuilder("TXBEG", "");

			// 1.2.10 Operative Information
			// Fill with blank spaces if the length of command is not the multiple of 4
			byte operativeInfo[] = operationInfoBuilder(Globals.login_command, null);

			// 1.2.11 Checksum (8 Byte){Perform XOR for "Message header + Session header + Transaction header + Operative information"}
			byte checkSum[] = checkSumBuilder(msgHeader, sessionHeader, transactionHeader, operativeInfo);

			// 1.2.6 Message Length (4 Byte)
			byte msgLength[] = msgLengthBuilder(msgHeader, sessionHeader, transactionHeader, operativeInfo);

			byte msgS[] = msgBuilder(msgStartFlag, msgLength, msgHeader, sessionHeader, transactionHeader, operativeInfo, checkSum);

			log.debug(printf(msgS));
			//log.info(new String(msgS));

			//log.info("Login Msg is sending to Server.");
			byte b2[] = operationCall(msgS);

			//log.debug("Receive as Login Response : : " + printf(b2));
			//log.info("Receive as Login Response : : " + new String(b2));
			System.out.println("Receive as Login Response : : " + new String(b2));

			//Map map = operationRespParser(b2);
			String respMsg = new String(b2);
			if(respMsg.contains("RETN=0")){
				return true;
			}
			if(respMsg.contains("RETN=5")){
				return true;
			}
			//			String RETN = (String) map.get("RETN");
			//			if (RETN != null && RETN.equals("0")) {
			//				log.debug("RETN : " + RETN);
			//				return true;
			//			}
		} catch (IOException e) {
			e.printStackTrace();
			//log.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			//log.error(e.getMessage(), e);
			throw e;
		}

		return false;
	}

	private String printf(byte b[]) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < b.length; i++) {
			sb.append(Integer.toHexString((int) (b[i])));
			sb.append(" ");
		}
		return sb.toString();
	}

	private String printf(Map map) {
		StringBuffer sb = new StringBuffer();
		Set set = map.keySet();
		for (Iterator it = set.iterator(); it.hasNext();) {
			String key = (String) it.next();
			sb.append(key);
			sb.append("=");
			sb.append(map.get(key));
			sb.append("|");
		}
		return sb.toString();
	}

	private byte[] checkSumBuilder(byte[] msgHeader, byte[] sessionHeader, byte[] transactionHeader, byte[] operativeInfo) {
		// 1.2.11 Checksum (8 Byte){Perform XOR for "Message header + Session header + Transaction header + Operative information"}
		byte checkSum[] = new byte[8];
		int Length = 0x00;

		byte p_calc[] = new byte[msgHeader.length + sessionHeader.length + transactionHeader.length + operativeInfo.length];
		Length = 0x00;

		System.arraycopy(msgHeader, 0x00, p_calc, Length, msgHeader.length);
		Length += msgHeader.length;

		System.arraycopy(sessionHeader, 0x00, p_calc, Length, sessionHeader.length);
		Length += sessionHeader.length;

		System.arraycopy(transactionHeader, 0x00, p_calc, Length, transactionHeader.length);
		Length += transactionHeader.length;

		System.arraycopy(operativeInfo, 0x00, p_calc, Length, operativeInfo.length);
		Length += operativeInfo.length;

		byte hex_asc[] = "0123456789ABCDEF".getBytes();

		int sum_c[] = new int[4];
		for (int i = 0; i < 4; i++)
			sum_c[i] = 0;

		int chksum_len = p_calc.length;

		for (int i = 0; i < chksum_len; i += 4) {
			sum_c[i % 4] ^= p_calc[i + 0];
			sum_c[i % 4 + 1] ^= p_calc[i + 1];
			sum_c[i % 4 + 2] ^= p_calc[i + 2];
			sum_c[i % 4 + 3] ^= p_calc[i + 3];
		}

		for (int i = 0; i < 4; i++) {
			sum_c[i] = (~sum_c[i]) & 0x0ff;
			checkSum[i * 2] = hex_asc[sum_c[i] >> 4];
			checkSum[i * 2 + 1] = hex_asc[sum_c[i] & 0x0f];
		}

		return checkSum;
	}
}
