package loggerDEmo;

import org.apache.log4j.Logger;

public class ExternalService extends InternalService {
	
	private final static Logger logger = Logger.getLogger(ExternalService.class);
	private String input;
	private String output;
	public String getInput() {
		return input;
	}
	public void setInput(String input) {
		this.input = input;
	}
	public String getOutput() {
		return output;
	}
	public void setOutput(String output) {
		this.output = output;
	}
	
	
	public void service(){
		logger.info("External Service Request: "+input);
		setOutput("Hi "+input); // Here your SErvice call to External Service
		logger.info("External Service Response: "+output);
		super.setInput("Vivek");
		super.service();
	}

}
