package com.demo.demo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONUtils {
	static List<String> compareAttribute= Arrays.asList("password,requestId,timeStamp,userName,totalAmountAllowed,callBack,minAmountAllowed,maxAmountAllowed");
	public static boolean jsonsEqual(Object obj1, Object obj2,Map differenceValue ) throws JSONException {
	
		if (!obj1.getClass().equals(obj2.getClass())) {
			return false;
		}
		boolean b= false;

		if (obj1 instanceof JSONObject) {
			JSONObject jsonObj1 = (JSONObject) obj1;
			JSONObject jsonObj2 = (JSONObject) obj2;
			String[] names = JSONObject.getNames(jsonObj1);
			String[] names2 = JSONObject.getNames(jsonObj2);
			if (names.length != names2.length) {
				return false;
			}else {
				List l2= validateArray(names,names2);
				if(l2.size()>0) {
Iterator i1= l2.iterator();
while(i1.hasNext()) {
	String data=(String) i1.next();
	if(jsonObj1.toString().contains(data)) {
		differenceValue.put(data, jsonObj1.get(data).toString());
	}else {
		differenceValue.put(data, jsonObj2.get(data).toString());
	}
}
				}

			}

			for (String fieldName : names) {
				if(compareAttribute.toString().contains(fieldName)) {
					Object obj1FieldValue = jsonObj1.get(fieldName);
					Object obj2FieldValue = jsonObj2.get(fieldName);

					if (!jsonsEqual(obj1FieldValue, obj2FieldValue,differenceValue)) {
						differenceValue.put(fieldName, obj2FieldValue.toString());
					}
				}
			}
			if(b) {
				return false;
			}
		} else if (obj1 instanceof JSONArray) {
			JSONArray obj1Array = (JSONArray) obj1;
			JSONArray obj2Array = (JSONArray) obj2;

			if (obj1Array.length() != obj2Array.length()) {
				return false;
			}

			for (int i = 0; i < obj1Array.length(); i++) {
				boolean matchFound = false;

				for (int j = i; j <=i; j++) {
					if (!jsonsEqual(obj1Array.get(i), obj2Array.get(j),differenceValue)) {
						matchFound=true;
					}
				}

				if (matchFound) {
					return false;
				}
			}
		} else {
			if (!obj1.equals(obj2)) {
				return false;
			}
		}

		return true;
	}

	private static List validateArray(String[] a, String[] b) {
		List l1 = new ArrayList();
		Arrays.sort(a);
		Arrays.sort(b);
		List<String> list1 = Arrays.asList(a);
		List<String> list2 = Arrays.asList(b);
		Set<String> union = new HashSet<String>(list1);
		union.addAll(list2);
		Set<String> intersection = new HashSet<String>(list1);
		intersection.retainAll(list2);
		union.removeAll(intersection);
		for (String n : union) {
			l1.add(n);
		}
		return l1;
	}
}
