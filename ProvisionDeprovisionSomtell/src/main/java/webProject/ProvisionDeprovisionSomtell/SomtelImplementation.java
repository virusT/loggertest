package webProject.ProvisionDeprovisionSomtell;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import webProject.ProvisionDeprovision.ProvisionData;
import webProject.ProvisionDeprovision.ProvisionDeprovision;

public class SomtelImplementation implements ProvisionDeprovision {

	private static HttpURLConnection urlconn = null;
	private static BufferedOutputStream out = null;
	private static BufferedInputStream inputStream = null;
	private static Scanner input = new Scanner(System.in);
	private static int count=0;
	
	private Logger logger = LogManager.getLogger(SomtelImplementation.class.getName());

	public ProvisionData loginRequest(Object data) {
		ProvisionData dto=(ProvisionData) data; 
		String resXML=null;
		try {
			String xml ="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">" + 
					"<soapenv:Body>" + 
					"              <LGI>" + 
					"			  <OPNAME>"+dto.getUserName()+"</OPNAME>" + 
					"			  <PWD>"+dto.getPassword()+"</PWD>" + 
					"			  </LGI>" + 
					"              </soapenv:Body>" + 
					"</soapenv:Envelope>";
			logger.info("XML::::" + xml);
			HttpURLConnection.setFollowRedirects(false);
			urlconn = (HttpURLConnection) new URL(dto.getLoginUrl().toString()).openConnection();
			logger.info("Connection " + urlconn);
			urlconn.setRequestMethod("POST");
			urlconn.setRequestProperty("Accept", "*/*");
			urlconn.setRequestProperty("Connection", "Keep-Alive");
			urlconn.setRequestProperty("Content-type", "text/xml;charset=UTF-8");
			urlconn.setRequestProperty("User-Agent", "Jakarta Commons-HttpClient/3.1");
			urlconn.setReadTimeout(30000);
			urlconn.setConnectTimeout(30000);
			int length = xml.getBytes().length;
			urlconn.setRequestProperty("Content-Length", "" + xml.length());

			urlconn.setDoInput(true);
			urlconn.setDoOutput(true);
			out = new BufferedOutputStream(urlconn.getOutputStream());
			out.write(xml.getBytes());
			out.flush();

			int httpresonse = urlconn.getResponseCode();
			if (httpresonse == 307) {
				try {
					BufferedInputStream inputStream = new BufferedInputStream(urlconn.getInputStream());
					logger.info("creating stream **********************************************");
					StringBuffer sb = new StringBuffer();
					int character = -1;
					while ((character = inputStream.read()) != -1) {
						sb.append((char) character);
					}
					Map<String, List<String>> map = urlconn.getHeaderFields();
					for (Map.Entry<String, List<String>> entry : map.entrySet()) {
						logger.info("Header Information  key " + entry.getKey());
						logger.info("Value" + entry.getValue());
						if(entry.getKey()!=null  &&  entry.getKey().trim().equalsIgnoreCase("Location")){
							dto.setProvisionUrl(entry.getValue().toString().replace("[", "").replace("]", ""));
							dto.setDeprovisionUrl(entry.getValue().toString().replace("[", "").replace("]", ""));
							logger.info("ProvisionUrl" + entry.getValue().toString().replace("[", "").replace("]", ""));
						}
					}
					logger.info("creating response **********************************************");
					resXML = sb.toString();
					logger.info("REsponse from System" +resXML.toString());
					dto.setStatusCode("200");
					dto.setStatus("0");
					logger.info("Success Response::::" + httpresonse);
				}catch (Exception e) {
					logger.error(e.getMessage(),e);
					dto.setStatusCode("400");
					dto.setStatus("1");
				}finally {
					if(inputStream!=null) {
						inputStream.close();
					}
				}
			} else {
				dto.setStatusCode(String.valueOf(httpresonse));
				dto.setStatus("1");
				logger.info("Error Response::::" + httpresonse);
			}
		} catch (Exception e) {
			dto.setStatusCode("400");
			dto.setStatus("1");
			logger.error(e.getMessage(),e);
			e.printStackTrace();
		} finally {
			try {
				if (input != null) {
					input = null;
				}
				if (inputStream != null)
					inputStream.close();
				if (out != null) {
					out.close();
				out = null;}
				if (urlconn != null) {
					urlconn.disconnect();
					urlconn = null;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		logger.info("End of login::::");
		return dto;
	}

	public ProvisionData goProvision(Object dto) {
		boolean provision=true;
		ProvisionData data=(ProvisionData) dto;
		try {
			for(int i =0;i<data.getCount();i++) {
				provision(data);
				if(data.getResponse().contains("ERR5004:Session ID invalid or time out")) {
					logger.info("Provisioning Response one by one " +data.getResponse());
				}else {
					logger.info("Provisioning Response" +data.getResponse());
					break;
				}
			}
		}catch (Exception e) {
			data.setStatus("1");
			data.setStatusCode("400");
			logger.error(e.getMessage(),e);
		}finally {
		}
		return data;
	}
	
	private ProvisionData provision(ProvisionData data) {
		String resXML=null;

		try {
			String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">" + 
					"              <soapenv:Body>" + 
					"                            <MOD_PLMNSS>" + 
					"                                          <ISDN>"+data.getMsidn()+"</ISDN>" + 
					"                                          <PROV>TRUE</PROV>" + 
					"                                          <PLMNSPECSS>PLMN-SS-E</PLMNSPECSS>" + 
					"                            </MOD_PLMNSS>" + 
					"              </soapenv:Body>" + 
					"</soapenv:Envelope>";

			logger.info("Start of Provisioning::::"+"XML::::" + xml);
			HttpURLConnection.setFollowRedirects(false);
			urlconn = (HttpURLConnection) new URL(data.getProvisionUrl().toString()).openConnection();
			urlconn.setRequestMethod("POST");

			urlconn.setRequestProperty("Accept", "*/*");
			urlconn.setRequestProperty("Connection", "Keep-Alive");
			urlconn.setRequestProperty("Content-type", "text/xml;charset=UTF-8");
			urlconn.setRequestProperty("User-Agent", "Jakarta Commons-HttpClient/3.1");
			urlconn.setReadTimeout(30000);
			urlconn.setConnectTimeout(30000);

			int length = xml.getBytes().length;
			urlconn.setRequestProperty("Content-Length", "" + xml.length());

			urlconn.setDoInput(true);
			urlconn.setDoOutput(true);

			out = new BufferedOutputStream(urlconn.getOutputStream());
			out.write(xml.getBytes());
			out.flush();

			int httpresonse = urlconn.getResponseCode();

			if (httpresonse == 200) {
				try {
				BufferedInputStream inputStream = new BufferedInputStream(urlconn.getInputStream());
				StringBuffer sb = new StringBuffer();
				int character = -1;
				while ((character = inputStream.read()) != -1) {
					sb.append((char) character);
				}
				resXML = sb.toString();
				data.setResponse(resXML);
				if(resXML.contains("Operation is successful")){
					data.setStatusCode(String.valueOf(httpresonse));
					data.setStatus("0");	
				}else {
					data.setStatus("1");	
				}

				logger.info("Success Response::::" + resXML);
				}
				catch (Exception e) {
					logger.error(e.getMessage(),e);
					data.setStatusCode("400");
					data.setStatus("1");
				}finally {
					if(inputStream!=null) {
						inputStream.close();
					}
				}
			} else {
				try {
					BufferedInputStream inputStream = new BufferedInputStream(urlconn.getErrorStream());
					StringBuffer sb = new StringBuffer();
					int character = -1;
					while ((character = inputStream.read()) != -1) {
						sb.append((char) character);
					}
					data.setStatusCode(String.valueOf(httpresonse));
					data.setStatus("1");
					resXML = sb.toString();
					data.setResponse(resXML);
					logger.info("Error Response::::" + resXML);
					
				}catch (Exception e) {
					logger.error(e.getMessage(),e);
					data.setStatusCode("400");
					data.setStatus("1");
				}finally {
					if(inputStream!=null) {
						inputStream.close();
					}
				}
			}
		} catch (Exception e) {	
			data.setStatusCode("400");
			data.setStatus("1");
			logger.error(e.getMessage(),e);
			e.printStackTrace();
		} finally {
			try {
				if (input != null) {
					input = null;
				}
				if (inputStream != null)
					inputStream.close();
				if (out != null)
					out.close();
				out = null;
				if (urlconn != null) {
					urlconn.disconnect();
					urlconn = null;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		logger.info("End of Provisioning::::XML::::");
		return data;
	}

	public ProvisionData getHLRStatus(Object dto) {
		return null;}

	public ProvisionData goDeprovisioin(Object dto) {
		ProvisionData data=(ProvisionData) dto;
		try {
			for(int i =0;i<data.getCount();i++) {
				Deprovisioin(data);
				if(data.getResponse().contains("ERR5004:Session ID invalid or time out")) {
					logger.info("DeProvisioning Response one by one " +data.getResponse());
				}else {
					logger.info("DeProvisioning Response" +data.getResponse());
					break;
				}
			}
		}catch (Exception e) {
			data.setStatus("1");
			data.setStatusCode("400");
			logger.error(e.getMessage(),e);
		}finally {
		}
		return data;
	}

	public ProvisionData Deprovisioin(ProvisionData data) {
		String resXML=null;
		try {
			String xml =  
					"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">" + 
					"              <soapenv:Body>" + 
					"                            <MOD_PLMNSS>" + 
					"                                          <ISDN>"+data.getMsidn()+"</ISDN>" + 
					"                                          <PROV>FALSE</PROV>" + 
					"                                          <PLMNSPECSS>PLMN-SS-E</PLMNSPECSS>" + 
					"                            </MOD_PLMNSS>" + 
					"              </soapenv:Body>" + 
					"</soapenv:Envelope>";

			logger.info("XML::::" + xml);
			HttpURLConnection.setFollowRedirects(false);
			urlconn = (HttpURLConnection) new URL(data.getDeprovisionUrl().toString()).openConnection();
			urlconn.setRequestMethod("POST");

			urlconn.setRequestProperty("Accept", "*/*");
			urlconn.setRequestProperty("Connection", "Keep-Alive");
			urlconn.setRequestProperty("Content-type", "text/xml;charset=UTF-8");
			urlconn.setRequestProperty("User-Agent", "Jakarta Commons-HttpClient/3.1");
			urlconn.setReadTimeout(30000);
			urlconn.setConnectTimeout(30000);
			
			urlconn.setRequestProperty("Content-Length", "" + xml.length());

			urlconn.setDoInput(true);
			urlconn.setDoOutput(true);

			out = new BufferedOutputStream(urlconn.getOutputStream());
			out.write(xml.getBytes());
			out.flush();

			int httpresonse = urlconn.getResponseCode();

			if (httpresonse == 200) {
				try {
					BufferedInputStream inputStream = new BufferedInputStream(urlconn.getInputStream());
					StringBuffer sb = new StringBuffer();
					int character = -1;
					while ((character = inputStream.read()) != -1) {
						sb.append((char) character);
					}
					resXML = sb.toString();
					data.setResponse(resXML);
					if(resXML.contains("Operation is successful")){
						data.setStatusCode(String.valueOf(httpresonse));
						data.setStatus("0");	
					}else {
						data.setStatus("1");	
					}

					logger.info("Success Response::::" + resXML);
				}
				catch (Exception e) {
					logger.error(e.getMessage(),e);
					data.setStatusCode("400");
					data.setStatus("1");
				}finally {
					if(inputStream!=null) {
						inputStream.close();
					}
				}} else {
					try {
						BufferedInputStream inputStream = new BufferedInputStream(urlconn.getErrorStream());
						StringBuffer sb = new StringBuffer();
						int character = -1;
						while ((character = inputStream.read()) != -1) {
							sb.append((char) character);
						}
						data.setStatusCode(String.valueOf(httpresonse));
						data.setStatus("1");
						resXML = sb.toString();
						data.setResponse(resXML);
						logger.info("Error Response::::" + resXML);

					}catch (Exception e) {
						logger.error(e.getMessage(),e);
						data.setStatusCode("400");
						data.setStatus("1");
					}finally {
						if(inputStream!=null) {
							inputStream.close();
						}
					}
				}
		} catch (Exception e) {
			data.setStatusCode("400");
			data.setStatus("1");
			logger.error(e.getMessage(),e);
			e.printStackTrace();
		} finally {
			try {
				if (input != null) {
					input = null;
				}
				if (inputStream != null)
					inputStream.close();
				if (out != null)
					out.close();
				out = null;
				if (urlconn != null) {
					urlconn.disconnect();
					urlconn = null;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		logger.info("End of deprovisioning::::"+"XML::::" + resXML);
		return data;
	}

	public ProvisionData goLogout(Object dto) {
		String resXML=null;
		ProvisionData data=(ProvisionData) dto;

		try {
			String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">" + 
					"<soapenv:Body>" + 
					"<LGO>" + 
					"</LGO>" + 
					"</soapenv:Body>" + 
					"</soapenv:Envelope>";

			logger.info("Start of logOut::::" +"XML::::" + xml);
			HttpURLConnection.setFollowRedirects(false);
			urlconn = (HttpURLConnection) new URL(data.getLogoutUrl().toString()).openConnection();
			urlconn.setRequestMethod("POST");
			urlconn.setRequestProperty("Accept", "*/*");
			urlconn.setRequestProperty("Connection", "Keep-Alive");
			urlconn.setRequestProperty("Content-type", "text/xml;charset=UTF-8");
			urlconn.setRequestProperty("User-Agent", "Jakarta Commons-HttpClient/3.1");
			urlconn.setReadTimeout(30000);
			urlconn.setConnectTimeout(30000);

			int length = xml.getBytes().length;
			urlconn.setRequestProperty("Content-Length", "" + xml.length());

			urlconn.setDoInput(true);
			urlconn.setDoOutput(true);

			out = new BufferedOutputStream(urlconn.getOutputStream());
			out.write(xml.getBytes());
			out.flush();

			int httpresonse = urlconn.getResponseCode();

			if (httpresonse == 200) {
				logger.info("Success Response::::" + httpresonse);
				data.setStatusCode(String.valueOf(httpresonse));
				data.setStatus("0");
			} 
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (input != null) {
					input = null;
				}
				if (inputStream != null)
					inputStream.close();
				if (out != null)
					out.close();
				out = null;
				if (urlconn != null) {
					urlconn.disconnect();
					urlconn = null;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		logger.info("End of logOut::::");
		return data;
	}
}
